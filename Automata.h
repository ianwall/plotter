#pragma once

#include <stdint.h>
#include <string.h>
#include "Path.h"
#include <set>
#include <vector>

#include "Program.h"

struct Edge {
    Edge(int _s0, int _c0, int _s1, int _c1) {
        if(_s1 < _s0) {
            s0 = _s1; c0 = _c1;
            s1 = _s0; c1 = _c0;
        } else {
            s0 = _s0; c0 = _c0;
            s1 = _s1; c1 = _c1;
        }
    }

    bool operator<(Edge o) const {
        if(s0 != o.s0) return (s0 < o.s0);
        if(s1 != o.s1) return (s1 < o.s1);
        if(c0 != o.c0) return (c0 < o.c0);
        return (c1 < o.c1);
    }

    int c0, s0;
    int c1, s1;
};

class Automata : public Program {
public:
    bool Cyclical, random;

    Automata() 
    {
        Cyclical = random = false;
        NumInitialized = 12;
        C_max = 4055;
        Rule = 30;
        Width = 156;
        Steps = 130;
        Grid = nullptr;
    }

    Automata(int rule, int width, int steps) 
        : Automata()
    {
        Rule = rule;
        Width = width;
        Steps = steps;
    }

    ~Automata() {
        delete[] Grid;
    }

    virtual char* GetName() {
        return "Automata";
    }

    virtual void BeginFrame() {
        ImGui::Begin("Automata");
        bool change = false;
        change |= ImGui::InputInt("Rule", &Rule);
        change |= ImGui::SliderInt("Width", &Width, 0, 500);
        change |= ImGui::SliderInt("Steps", &Steps, 0, 500);
        change |= ImGui::SliderInt("neighborhood", &C_max, 10, 5500);
        change |= ImGui::SliderInt("Initialized", &NumInitialized, 1, 100);
        change |= ImGui::Checkbox("Randomize", &random);
        change |= ImGui::Checkbox("Cyclical", &Cyclical);

        if (change) {
            delete[] Grid;
            Grid = nullptr;
            path.resize(0);
        }
    }


    virtual void EndFrame() {
        ImGui::End();
    }

    virtual std::vector<Path>& GetPathes() { 
        if (Grid == nullptr) {
            genPathes();
        }

        return path; 
    }

private:
    void genPathes() {
        Grid = new uint8_t[Width*Steps];
        memset(Grid, 0, sizeof(uint8_t)*Width*Steps);

        float step = 1.0 / (NumInitialized + 1);
        for (int j = 0; j < NumInitialized; ++j) {
            if(random)
                set(0, (Width*rand())/RAND_MAX, 1);
            else
                set(0, Width*((j+1)*step), 1);
        }
        for (int i = 1; i < Steps; ++i) {
            for (int j = 0; j < Width; ++j) {
                set(i, j, (Rule >> group(i - 1, j)) & 1);
            }
        }

        std::set<Edge> edges;
        for (int s = 0; s < Steps-1; ++s) {
            for (int c = 0; c < Width; ++c) {
                if(read(s,c)) {
                    if(read(s+1,c-1)) edges.emplace(s,c, s+1, c-1);
                    if(read(s+1,c))   edges.emplace(s,c, s+1, c);
                    if(read(s+1,c+1)) edges.emplace(s,c, s+1, c+1);
                }
            }
        }

        int dirs[6][2] = {{-1,1},{0,1},{1,1},{-1,-1},{0,-1},{1,-1}};
        int dir_start = 0;
        int dir = 0;

        Edge e = *edges.begin();
        while(!edges.empty()) {
            auto it_a = edges.lower_bound(e);
            auto it_b = edges.upper_bound(e);

            int C = 0;
            while(it_a != edges.begin() && C < C_max) { --it_a; ++C; }

            C=0;
            while(it_b != edges.end() && C < C_max) { ++it_b; ++C; }

            Edge best = *it_a;
            int dist = abs(best.c0-e.c0)+abs(best.s0-e.s0);
            
            for(auto it = it_a; it != it_b; ++it) {
                Edge f = *it;
                int _dist = abs(f.c0-e.c0)+abs(f.s0-e.s0);
                if(_dist < dist) {
                    dist = dist;
                    best = f;
                }
            }

            float Wf = float(Width-1);
            float Sf = float(Steps-1);
            e = best; //*it;
            path.emplace_back();
            path.back().emplace_back(MOVE, e.c0/Wf, e.s0/Sf);
            int c = e.c0;
            int s = e.s0;

            dir = (dir + 1)%6;
            int tries = 0;

            while(tries < 6) {
                int cdir = dirs[dir][0];
                int sdir = dirs[dir][1];
                dir = (dir+1)%6;
                e = Edge(s,c, s+sdir, c+cdir);
                if(edges.count(e) > 0) {
                    path.back().emplace_back(LINE, 
                        (c+cdir)/Wf, (s+sdir)/Sf);
                    tries = 0;
                    c = c+cdir;
                    s = s+sdir;
                    edges.erase(e);
                } else {
                    tries++;
                    //break;
                }   
            }
        }
    }

    std::vector<Path> path;

    // Get 3 neighborhood of j
    uint8_t group(int i, int j) {
        return (read(i,j-1)<<2)|(read(i,j)<<1)|(read(i,j+1));
    }

    uint8_t read(int s, int c) const {
        if(c < 0 || c >= Width)
            if(Cyclical)
                return Grid[s*Width+(c+Width)%Width];
            else
                return 0;
        
        return Grid[s*Width+c];
    }

    uint8_t& get(int i, int j) const {
        return Grid[i*Width+j];
    }

    void set(int i, int j, uint8_t v) const {
        Grid[i*Width+j] = v;
    }

    uint8_t* Grid;
    int Rule;
    int Width;
    int Steps;

    int NumInitialized = 12;
    int C_max = 4055;
};