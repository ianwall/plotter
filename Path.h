#pragma once
#include <vector>

enum Op {
    MOVE, LINE, CUBIC, QUAD, SCUBIC, SQUAD, ARC, CLOSE
};

struct Instruction {
    Instruction(Op o) 
        : op(o), f{0,0,0,0,0,0} {}

    Instruction(Op o, float f0)
        : op(o), f{f0,0,0,0,0,0} {}

    Instruction(Op o, float f0, float f1)
        : op(o), f{f0,f1,0,0,0,0} {}

    Instruction(Op o, float f0, float f1, float f2)
        : op(o), f{f0,f1,f2,0,0,0} {}

    Instruction(Op o, float f0, float f1, float f2, float f3)
        : op(o), f{f0,f1,f2,f3,0,0} {}

    Instruction(Op o, float f0, float f1, float f2, float f3, float f4) 
        : op(o), f{f0,f1,f2,f3,f4,0} {}

    Instruction(Op o, float f0, float f1, float f2, float f3, float f4, float f5)
        : op(o), f{f0,f1,f2,f3,f4,f5} {}

    Op op;
    float f[6];
};

typedef std::vector<Instruction> Path;
