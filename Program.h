#pragma once

#include <stdint.h>
#include <string.h>
#include "Path.h"
#include <set>
#include <vector>

class Program {
public:
    virtual char* GetName() = 0;
    virtual void BeginFrame() = 0;
    virtual std::vector<Path>& GetPathes() = 0;
    virtual void EndFrame() = 0;

private:
};