#pragma once

#include <stdint.h>
#include <string.h>
#include "Path.h"
#include <set>
#include <vector>
#include "Program.h"

class Testing : public Program {
public:
    Testing() {}

    virtual char* GetName() { return "Testing Ground"; }

    virtual void BeginFrame() {
        ImGui::Begin("Testing Ground");
        int Rule = 10;
        ImGui::InputInt("Rule", &Rule);
    }

    virtual std::vector<Path>& GetPathes() {
        return path;
    }

    virtual void EndFrame() {
        ImGui::End();
    }

private:
    std::vector<Path> path;

};