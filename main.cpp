
#include <imgui.h>
#include "imgui_impl_glfw_gl3.h"
#include <stdio.h>
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>
#include "nanovg.h"
#include "nanovg_gl.h"
#include "SVG.h"

#include "Automata.h"
#include "Testing.h"

int rule = 30;
int cells = 156;
int steps = 130;
bool drawUp = false;
bool drawOut = false;
float width = 2.5f;
ImVec4 draw = ImVec4(1.0, 1.00f, 1.00f, 1.00f);
ImVec4 clear = ImVec4(0.0f, 0.0f, 0.0f, 1.00f);

struct NVGcontext* vg;
std::vector<Program*> programs;
Program* program;

void InitPrograms() {
    programs.push_back(new Automata(rule, cells, steps));
    programs.push_back(new Testing());
}

void chooseOptions()
{
    ImGui::Begin("Global");

    static int chosenProgram = 0;
    char** plist = new char*[programs.size()];
    char** pIt = plist;
    for (Program* p : programs) {
        *(pIt++) = p->GetName();
    }
    ImGui::ListBox("Program", &chosenProgram, plist, programs.size());
    program = programs[chosenProgram];

    ImGui::SliderFloat("Stroke Width", &width, 0.0f, 100.0);
    ImGui::ColorEdit3("draw color", (float*)&draw);
    ImGui::ColorEdit3("clear color", (float*)&clear);
    ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    ImGui::Checkbox("Draw up pathes", &drawUp);
    drawOut = ImGui::Button("Draw Out"); 
}

void DrawFrame(float width, float height) {
    program->BeginFrame();
    auto pathes = program->GetPathes();
    program->EndFrame();

    int npath = 0;
    int ninst = 0;
    float px,py,ox,oy,tx,ty;
    px=py=ox=oy=0.0f;
    float uplength = 0.0f;

    nvgBeginPath(vg);
    nvgMoveTo(vg, 0, 0); 
    
    int WMM = XMM*STEPS;
    int HMM = YMM*STEPS;
    SVG* svg;
    if(drawOut) {
        svg = new SVG();
        svg->BeginPath();
        svg->MoveTo(0,0);
    }

    for(Path path : pathes) {
        ++npath;
        for(Instruction i : path) {
            switch(i.op) {
                case MOVE: 
                    if(drawUp) {
                        nvgStroke(vg);
                        nvgSave(vg);

                        nvgBeginPath(vg);
                        nvgMoveTo(vg, width*ox, height*oy);
                        nvgLineTo(vg, width*i.f[0], height*i.f[1]); 
                        uplength += width*fabs(ox-i.f[0]) + height*fabs(oy-i.f[1]);
                        nvgStrokeColor(vg, nvgRGBAf(1.0,0.0,0.0,0.5));
                        nvgStroke(vg);

                        nvgRestore(vg);
                        nvgBeginPath(vg);
                    }
                    if(drawOut) svg->MoveTo(i.f[0], i.f[1]);
                    nvgMoveTo(vg, width*i.f[0], height*i.f[1]); 
                    px = ox = i.f[0];
                    py = oy = i.f[1];
                    break;
                case LINE: 
                    if(drawOut) svg->LineTo(i.f[0], i.f[1]);
                    nvgLineTo(vg, width*i.f[0], height*i.f[1]); 
                    px = ox = i.f[0];
                    py = oy = i.f[1];
                    break;
                case CUBIC: 
                    if(drawOut) svg->CubicTo(i.f[0], i.f[1], 
                                             i.f[2], i.f[3], 
                                             i.f[4], i.f[5]);
                    nvgBezierTo(vg, 
                        width*i.f[0], height*i.f[1], 
                        width*i.f[2], height*i.f[3], 
                        width*i.f[4], height*i.f[5]); 
                    px = i.f[2]; py = i.f[3];
                    ox = i.f[4]; oy = i.f[5];
                    break;
                case QUAD: 
                    if(drawOut) svg->QuadTo(i.f[0], i.f[1], 
                                            i.f[2], i.f[3]);
                    nvgQuadTo(vg, 
                        width*i.f[0], height*i.f[1], 
                        width*i.f[2], height*i.f[3]);
                    px = i.f[0]; py = i.f[1];
                    ox = i.f[2]; oy = i.f[3];
                    break;
                case ARC: 
                    nvgArcTo(vg, i.f[0], i.f[1], i.f[2], i.f[3], i.f[4]); 
                    px = ox = i.f[2];
                    py = oy = i.f[3];
                    break;
                case SQUAD:
                    tx = (2*ox-px);
                    ty = (2*oy-py);
                    if(drawOut) svg->QuadTo(tx, ty, 
                                            i.f[0], i.f[1]);
                    nvgQuadTo(vg, 
                        width*tx, height*ty,
                        width*i.f[0], height*i.f[1]);
                    px = tx; py = ty;
                    ox = i.f[0]; oy = i.f[1];
                    break;
                case SCUBIC:
                    if(drawOut) svg->CubicTo((2*ox-px), (2*oy-py), 
                                             i.f[0], i.f[1], 
                                             i.f[2], i.f[3]);
                    nvgBezierTo(vg, 
                        width*(2*ox-px), height*(2*oy-py), 
                        width*i.f[0], height*i.f[1], 
                        width*i.f[2], height*i.f[3]); 
                    px = i.f[0]; py = i.f[1];
                    ox = i.f[2]; oy = i.f[3];
                    break;
                case CLOSE: 
                    nvgClosePath(vg); 
                    break;
            }
            ++ninst;
        }
    }
    nvgStroke(vg);
    if(drawOut) {
        svg->EndPath();
        svg->WriteOut("test.svg");
        delete svg;
    }
    ImGui::Text("Pathes: %d, Inst: %d, Uplength: %.2f", npath, ninst, uplength);

    ImGui::End();
}


static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error %d: %s\n", error, description);
}

int main(int, char**)
{
    // Setup window
    InitPrograms();

    if (!glfwInit())
        return 1;

    glfwSetErrorCallback(error_callback);
#ifndef _WIN32 // don't require this on win32, and works with more cards
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);
    glfwWindowHint(GLFW_SAMPLES, 4);

    GLFWwindow* window = glfwCreateWindow(1280, 720, "ImGui OpenGL3 example", NULL, NULL);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1); // Enable vsync

    gl3wInit();

    vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);
    if (vg == NULL) {
        printf("Could not init nanovg.\n");
        return -1;
    }

    // Setup ImGui binding
    ImGui_ImplGlfwGL3_Init(window, true);

    // Setup style
    ImGui::StyleColorsClassic();
    //ImGui::StyleColorsDark();

    // Main loop
    while (!glfwWindowShouldClose(window))
    {
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        glfwPollEvents();
        ImGui_ImplGlfwGL3_NewFrame();

        chooseOptions(); 

        // Rendering
        int window_w, window_h;
        int display_w, display_h;
        glfwGetWindowSize(window, &window_w, &window_h);
        glfwGetFramebufferSize(window, &display_w, &display_h);
        float pxRatio = (float)display_w / (float)window_w;

        glViewport(0, 0, display_w, display_h);
        glClearColor(clear.x, clear.y, clear.z, clear.w);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

        {
            nvgBeginFrame(vg, display_w, display_h, pxRatio);
            nvgResetTransform(vg);

            nvgLineCap(vg, NVG_ROUND );
            nvgStrokeColor(vg, nvgRGBAf(draw.x,draw.y,draw.z,draw.w));
            nvgStrokeWidth(vg, width);

            DrawFrame(display_w, display_h);

            nvgEndFrame(vg);
        }

        // Cleanup state here?

        ImGui::Render();
        glfwSwapBuffers(window);
    }

    // Cleanup
    ImGui_ImplGlfwGL3_Shutdown();
    glfwTerminate();

    return 0;
}
