#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <algorithm>

//#define STEPS 127 // 2032
//#define XMM 300
//#define YMM 218
#define STEPS 1 // 2032
#define XMM 300
#define YMM 218

#define CLAMP(X) ((std::max)(0.0f, (std::min)(X, 1.0f)))

class SVG {
public:
    SVG() : pathID(4000) {
        output << "<svg" << std::endl
               << "  xmlns=\"http://www.w3.org/2000/svg\"" << std::endl
               << "  width=\""<<XMM<<"mm\"" << std::endl
               << "  height=\""<<YMM<<"mm\"" << std::endl
               << "  viewBox=\"-0.1 -0.1 1.2 1.2\"" << std::endl
               << "  preserveAspectRatio=\"none\"" << std::endl
               << "  >" << std::endl;

        output << "    <g stroke-width=\"0.002\" fill=\"none\" stroke=\"black\">" << std::endl;
        output << "    <rect x=\"0\" y=\"0\" width=\"1\" height=\"1\"/>" << std::endl;
    }

    void BeginPath() {
        output << "    <path" << std::endl
               << "      d=\"";
    }

    void MoveTo(float x, float y) {
        output << "M "<<CLAMP(x)<<","<<CLAMP(y)<<" ";
    }

    void LineTo(float x, float y) {
        output << "L "<<CLAMP(x)<<","<<CLAMP(y)<<" ";
    }

    void QuadTo(float x0, float y0, float x1, float y1) {
        output << "Q "<<CLAMP(x0)<<","<<CLAMP(y0)<<" "<<CLAMP(x1)<<","<<CLAMP(y1)<<" ";
    }

    void CubicTo(float x0, float y0, float x1, float y1, float x2, float y2) {
        output << "C "<<CLAMP(x0)<<","<<CLAMP(y0)<<" "<<CLAMP(x1)<<","<<CLAMP(y1)<<" "<<CLAMP(x2)<<","<<CLAMP(y2)<<" ";
    }

    void EndPath() {
        output << "\"\n    />" << std::endl;
    }

    void WriteOut(std::string file) {
        output << "    </g>" << std::endl;
        output << "</svg>" << std::endl;
        std::ofstream ofs;
        ofs.open (file, std::ofstream::out | std::ofstream::trunc);
        ofs << output.str();
        ofs.close();
    }

private:
    std::stringstream output;
    int pathID;
};